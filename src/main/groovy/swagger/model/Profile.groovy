package swagger.model

import io.swagger.annotations.ApiModelProperty

class Profile {
    @ApiModelProperty(value = "First name of the user.")
    String firstName = null

    @ApiModelProperty(value = "Last name of the user.")
    String lastName = null

    @ApiModelProperty(value = "Email address of the user")
    String email = null

    @ApiModelProperty(value = "Image URL of the user.")
    String picture = null

    @ApiModelProperty(value = "Promo code of the user.")
    String promoCode = null
}

