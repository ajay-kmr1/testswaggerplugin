package swagger.model

import io.swagger.annotations.ApiModelProperty

class Product {
    /**
     * Unique identifier representing a specific product for a given latitude & longitude. For example, uberX in San Francisco will have a different product_id than uberX in Los Angeles.
     * @return productId
     * */
    @ApiModelProperty(value = "Unique identifier representing a specific product for a given latitude & longitude. For example, uberX in San Francisco will have a different product_id than uberX in Los Angeles.")
    String productId = null

    @ApiModelProperty(value = "Description of product.")
    String description = null

    @ApiModelProperty(value = "Display name of product.")
    String displayName = null

    @ApiModelProperty(value = "Capacity of product. For example, 4 people.")
    String capacity = null

    @ApiModelProperty(value = "Image URL representing the product.")
    String image = null
}

