package swagger.model

import io.swagger.annotations.ApiModelProperty

public class Activity {
    @ApiModelProperty(value = "Unique identifier for the activity")
    String uuid = null
}

