package swagger.model

import io.swagger.annotations.ApiModelProperty

class Activities {
    @ApiModelProperty(value = "Position in pagination.")
    Integer offset = null

    @ApiModelProperty(value = "Number of items to retrieve (100 max).")
    Integer limit = null

    @ApiModelProperty(value = "Total number of items available.")
    Integer count = null

    @ApiModelProperty(value = "")
    List<Activity> history = new ArrayList<Activity>()
}

