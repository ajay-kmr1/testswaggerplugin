package testswaggerplugin

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(view: "/index")
        "500"(view: '/error')
        "404"(view: '/notFound')
        "/api/v1/city/$cityId"(controller: "city", action: "getCity", method: "GET")
        "/api/v1/city/list"(controller: "city", action: "getCityList", method: "GET")
        "/api/v1/city/createUpdate"(controller: "city", action: "createOrUpdateCity", method: "POST")
        "/api/v1/city/$cityId"(controller: "city", action: "deleteCity", method: "DELETE")
        "/api/v1/city/upload"(controller: "city", action: "uploadCityData", method: "POST")
    }
}
