package testswaggerplugin

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonNode
import com.github.fge.jackson.JsonLoader
import com.github.fge.jsonschema.core.exceptions.ProcessingException
import com.github.fge.jsonschema.core.report.ProcessingMessage
import com.github.fge.jsonschema.core.report.ProcessingReport
import com.github.fge.jsonschema.main.JsonSchema
import com.github.fge.jsonschema.main.JsonSchemaFactory
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import swagger.SwaggerService

class TestSwaggerDocumentService {

    SwaggerService swaggerService

    def serviceMethod() {

        Resource schemaResource = new ClassPathResource("schema_v2_0.json")
        String jsonSchema = schemaResource.getFile().getText('UTF-8')

        Resource validDocumentResource = new ClassPathResource("validDocument_v2_0.json")
        String validSwaggerDocument = validDocumentResource.getFile().getText('UTF-8')

        String generatedSwaggerDocument = swaggerService.generateSwaggerDocument()

        [validate(validSwaggerDocument, jsonSchema), validate(generatedSwaggerDocument, jsonSchema)]

    }

    void testValidate() {
        println("Starting Json Validation.")
        String jsonData = "\"Redemption\""
        String jsonSchema = "{ \"type\": \"string\", \"minLength\": 2, \"maxLength\": 11}"
        println(validate(jsonData, jsonSchema))

        jsonData = "Agony"  // Quotes not included
        println(validate(jsonData, jsonSchema))

        jsonData = "42"
        println(validate(jsonData, jsonSchema))
        jsonData = "\"A\""

        println(validate(jsonData, jsonSchema))
        jsonData = "\"The pity of Bilbo may rule the fate of many.\""
        println(validate(jsonData, jsonSchema))
    }

    Boolean validate(String jsonData, String jsonSchema) {
        ProcessingReport report = null
        Boolean result = Boolean.FALSE
        try {
            println("Applying schema: ${jsonSchema}\n data: ${jsonData}")
            JsonNode schemaNode = JsonLoader.fromString(jsonSchema)
            JsonNode data = JsonLoader.fromString(jsonData)
            JsonSchemaFactory factory = JsonSchemaFactory.byDefault()
            JsonSchema schema = factory.getJsonSchema(schemaNode)
            report = schema.validate(data)
        } catch (JsonParseException jpex) {
            println("Error. Something went wrong trying to parse json data: ${jsonData} \n or json schema: ${jsonSchema} \n Are the double quotes included? " + jpex.getMessage())
            //jpex.printStackTrace()
        } catch (ProcessingException pex) {
            println("Error. Something went wrong trying to process json data: ${jsonData}\n with json schema: ${jsonSchema}" + pex.getMessage())
            //pex.printStackTrace()
        } catch (IOException e) {
            println("Error. Something went wrong trying to read json data: ${jsonData}\n or json schema: ${jsonSchema}")
            //e.printStackTrace()
        }
        if (report != null) {
            Iterator<ProcessingMessage> iter = report.iterator()
            while (iter.hasNext()) {
                ProcessingMessage pm = iter.next()
                println("Processing Message: " + pm.getMessage())
            }
            result = report.isSuccess()
        }
        println(" Result=" + result)
        return result
    }
}
